package eu.tegie.league;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.view.View;
import android.widget.TextView;
import eu.tegie.league.fragments.NormalStatsFragment;
import eu.tegie.league.fragments.RankedStatsFragment;
import models.RiotObjects;

public class UserActivity extends BaseActivity {

    TextView username;
    RiotObjects summonerData;
    Fragment fragment;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        summonerData = (RiotObjects) getIntent().getSerializableExtra("SUMMONER_DATA");
        setDefaultFragment();

        username = (TextView)findViewById(R.id.user_activity_wins_score);
        username.setText(summonerData.summoner.getSummonerName());
    }

    public void changeFragment(View view)
    {
        Fragment fragment = new RankedStatsFragment();
        if (view == findViewById(R.id.user_activity_fragment_normal))  fragment = new NormalStatsFragment();
        if (view == findViewById(R.id.user_activity_fragment_aram))    fragment = new RankedStatsFragment();

        Bundle bundle = new Bundle();
        bundle.putLong("SUMMONER_ID",summonerData.summoner.getSummonerId());
        bundle.putString("SUMMONER_REGION", summonerData.summoner.getSummonerRegion());
        fragment.setArguments(bundle);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.user_activity_fragment_container, fragment);
        ft.commit();
    }

    private void setDefaultFragment()
    {
        fragment = new RankedStatsFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("SUMMONER_ID",summonerData.summoner.getSummonerId());
        bundle.putString("SUMMONER_REGION", summonerData.summoner.getSummonerRegion());
        fragment.setArguments(bundle);
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.user_activity_fragment_container, fragment);
        ft.commit();
    }

}
