package eu.tegie.league;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;

import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;


import java.io.IOException;
import java.io.Serializable;

import data.Parser.JSONSummonerParser;
import data.RequestBuilder;
import data.RiotHttpConnection;
import models.RiotObjects;
import okhttp3.OkHttpClient;
import utils.HttpRiotLinks;

public class MainActivity extends BaseActivity {

    RiotObjects riotObjects = new RiotObjects();
    private OkHttpClient client;
    ImageView submitButton;
    EditText summonerName;
    Spinner spinnerRegion;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        client = new OkHttpClient();
        summonerName = (EditText)findViewById(R.id.usernameEditText);
        submitButton = (ImageView)findViewById(R.id.button_main_activity);
        spinnerRegion = (Spinner)findViewById(R.id.regionSpinner);

        submitButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                String inputText = summonerName.getText().toString();
                String spinnerText = spinnerRegion.getSelectedItem().toString();
                DownloadClass download = new DownloadClass();
                download.execute(new String[]{inputText, spinnerText});
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private class DownloadClass extends AsyncTask<String,RiotObjects,RiotObjects>
    {

        @Override
        protected RiotObjects doInBackground(String... strings)
        {
            try {
                String data = RiotHttpConnection.GET(client, RequestBuilder.buildSummonerURL(strings[1], strings[0]));
                riotObjects = JSONSummonerParser.parseData(data, strings[0]);
                if (riotObjects != null) riotObjects.summoner.setSummonerRegion(strings[1]);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return riotObjects;
        }

        @Override
        protected void onPostExecute(RiotObjects riotObjects)
        {
            super.onPostExecute(riotObjects);

            if(riotObjects == null) {
                Toast.makeText(getBaseContext(),"Please enter valid user", Toast.LENGTH_LONG).show();
            } else {
                Intent intent = new Intent(MainActivity.this, UserActivity.class);
                intent.putExtra("SUMMONER_DATA", (Serializable) riotObjects);
                startActivity(intent);
            }
        }

    }

}
