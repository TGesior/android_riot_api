package eu.tegie.league.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.io.IOException;
import data.Parser.JSONSummonerParser;
import data.RequestBuilder;
import data.RiotHttpConnection;
import eu.tegie.league.R;
import models.RiotObjects;
import okhttp3.OkHttpClient;

public class RankedStatsFragment extends Fragment {

    private OkHttpClient client;
    RiotObjects riotObjects;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        client = new OkHttpClient();

        Bundle bundle = getArguments();
        if (bundle != null) {
            Long id = getArguments().getLong("SUMMONER_ID");
            String summonerId = Long.toString(id);
            String summonerRegion = getArguments().getString("SUMMONER_REGION");

            SummonerMatchHistory summonerMatchHistory = new SummonerMatchHistory();
            summonerMatchHistory.execute(summonerId,summonerRegion);
        }

        return inflater.inflate(R.layout.ranked_stats_fragment, container, false);
    }


    private class SummonerMatchHistory extends AsyncTask<String,Void,RiotObjects>
    {
        @Override
        protected RiotObjects doInBackground(String... strings)
        {
            try {
                String data = RiotHttpConnection.GET(client, RequestBuilder.buildSummonerMatchHistoryURL(strings[1], strings[0]));
                riotObjects = JSONSummonerParser.parseSummonerMatchHistory(data);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return riotObjects;
        }

        @Override
        protected void onPostExecute(RiotObjects riotObjects) {
            super.onPostExecute(riotObjects);

        }
    }
}
