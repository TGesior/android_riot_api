package models;

import java.io.Serializable;

public class Summoner implements Serializable{

    private long summonerId;
    private String summonerName;
    private int profileIconId;
    private long summonerLevel;
    private String summonerRegion;

    public int getProfileIconId() {
        return profileIconId;
    }

    public void setProfileIconId(int profileIconId) {
        this.profileIconId = profileIconId;
    }

    public long getSummonerId() {
        return summonerId;
    }

    public void setSummonerId(int summonerId) {
        this.summonerId = summonerId;
    }

    public String getSummonerName() {
        return summonerName;
    }

    public void setSummonerName(String summonerName) {
        this.summonerName = summonerName;
    }

    public long getSummonerLevel() {
        return summonerLevel;
    }

    public void setSummonerLevel(long summonerLevel) {
        this.summonerLevel = summonerLevel;
    }

    public String getSummonerRegion() {
        return summonerRegion;
    }

    public void setSummonerRegion(String summonerRegion) {
        this.summonerRegion = summonerRegion;
    }

}
