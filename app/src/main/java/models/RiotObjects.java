package models;

import java.io.Serializable;

public class RiotObjects implements Serializable {

    public Champion champion = new Champion();
    public Summoner summoner = new Summoner();
    public MatchHistory matchHistory = new MatchHistory();
}
