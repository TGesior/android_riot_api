package data;


import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.RequestBody;
import utils.HttpRiotLinks;

public class RequestBuilder {

    //Login request body
    public static RequestBody LoginBody(String username, String password, String token) {
        return new FormBody.Builder()
                .add("action", "login")
                .add("format", "json")
                .add("username", username)
                .add("password", password)
                .add("logintoken", token)
                .build();
    }

    public static HttpUrl buildSummonerURL(String region, String name) {
        return new HttpUrl.Builder()
                .scheme("https")
                .host(HttpRiotLinks.MAIN_URL)
                .addPathSegment("api")
                .addPathSegment("lol")
                .addPathSegment(region)
                .addPathSegment("v1.4")
                .addPathSegment("summoner")
                .addPathSegment("by-name")
                .addPathSegment(name)
                .addQueryParameter("api_key", HttpRiotLinks.API_KEY)
                .build();
    }

    public static HttpUrl buildSummonerMatchHistoryURL(String region, String id) {
        return new HttpUrl.Builder()
                .scheme("https")
                .host(HttpRiotLinks.MAIN_URL)
                .addPathSegment("api")
                .addPathSegment("lol")
                .addPathSegment(region)
                .addPathSegment("v1.3")
                .addPathSegment("game")
                .addPathSegment("by-summoner")
                .addPathSegment(id)
                .addPathSegment("recent")
                .addQueryParameter("api_key", HttpRiotLinks.API_KEY)
                .build();
    }

}
