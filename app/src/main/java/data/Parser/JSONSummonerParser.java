package data.Parser;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import models.RiotObjects;
import utils.Utils;

public class JSONSummonerParser {

    public static RiotObjects parseData(String data, String summonerName)
    {
        summonerName = Utils.makeLower(summonerName);
        RiotObjects riotObjects = new RiotObjects();

        try {
            //Głowny obiekt zawierajacy dane JSON
            JSONObject jsonObject = new JSONObject(data);
            //Obiekt JSON zawierajacy dane z obiektu JSON - nazwa uzytkownika
            JSONObject summonerObj = Utils.getObject(summonerName, jsonObject);
            riotObjects.summoner.setSummonerId(Utils.getInt("id", summonerObj));
            riotObjects.summoner.setProfileIconId(Utils.getInt("profileIconId", summonerObj));
            riotObjects.summoner.setSummonerLevel(Utils.getInt("summonerLevel", summonerObj));
            riotObjects.summoner.setSummonerName(Utils.getString("name", summonerObj));

            return riotObjects;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static RiotObjects parseSummonerMatchHistory(String data)
    {

        try{
            JSONObject jsonObject = new JSONObject(data);
            JSONArray jsonArray = jsonObject.getJSONArray("games");
            for (int i = 0; i < jsonArray.length(); i++) {
                RiotObjects riotObjects = new RiotObjects();
                JSONObject game = jsonArray.getJSONObject(i);

                JSONObject playerStats = Utils.getObject("stats", game);
                riotObjects.matchHistory.setTotalDamageDealtToChampions(Utils.getInt("totalDamageDealtToChampions", playerStats));
                //riotObjects.matchHistory.setItem0(Utils.getInt("item0", playerStats));
                //riotObjects.matchHistory.setItem1(Utils.getInt("item1", playerStats));
                //riotObjects.matchHistory.setItem2(Utils.getInt("item2", playerStats));
                //riotObjects.matchHistory.setItem3(Utils.getInt("item3", playerStats));
                //riotObjects.matchHistory.setItem4(Utils.getInt("item4", playerStats));
                //riotObjects.matchHistory.setItem5(Utils.getInt("item5", playerStats));
                //riotObjects.matchHistory.setItem6(Utils.getInt("item6", playerStats));
                riotObjects.matchHistory.setGoldEarned(Utils.getInt("goldEarned", playerStats));
                riotObjects.matchHistory.setPhysicalDamageDealtToChampions(Utils.getInt("physicalDamageDealtToChampions", playerStats));
                riotObjects.matchHistory.setLevel(Utils.getInt("level", playerStats));
                riotObjects.matchHistory.setNumDeaths(Utils.getInt("numDeaths", playerStats));
                riotObjects.matchHistory.setAssists((Utils.getInt("assists", playerStats)));
                riotObjects.matchHistory.setWin(Utils.getBoolean("win", playerStats));
                riotObjects.matchHistory.setChampionsKilled(Utils.getInt("championsKilled", playerStats));

                riotObjects.matchHistory.setIpEarned(Utils.getInt("ipEarned", game));
                riotObjects.matchHistory.setSpell1(Utils.getInt("spell1", game));
                riotObjects.matchHistory.setSpell2(Utils.getInt("spell2", game));
                riotObjects.matchHistory.setChampionId(Utils.getInt("championId", game));
                riotObjects.matchHistory.setLevel(Utils.getInt("level", game));



                return riotObjects;
            }

        }catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
