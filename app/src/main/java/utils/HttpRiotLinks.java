package utils;

public class HttpRiotLinks {

    public static final String API_KEY = "RGAPI-27ff537a-a28c-4a9b-889c-a8bc1be0129c";
    public static final String MAIN_URL = "na.api.pvp.net";
    public static final String SUMMONER_DATA_URL = "/api/lol/{region}/v1.4/summoner/by-name/{summonerNames}";
    public static final String SUMMONER_MASTERIES_URL = "/api/lol/{region}/v1.4/summoner/{summonerIds}/masteries";
    public static final String SUMMONER_RANKED_URL = "/api/lol/{region}/v1.3/stats/by-summoner/{summonerId}/ranked";
    public static final String SUMMONER_SUMMARY_URL = "/api/lol/{region}/v1.3/stats/by-summoner/{summonerId}/summary";
    public static final String SUMMONER_LEAGUE_URL = "/api/lol/{region}/v2.5/league/by-summoner/{summonerIds}";
    public static final String TEAM_LEAGUE_URL = "/api/lol/{region}/v2.5/league/by-team/{teamIds}";

}
